# PanelUsuariosView

#Metodologias implementadas

Durante la elaboracion de la parte de front-end de la utilice Angular, vale a clarar que el conocimiento que tengo sobre el mismo es muy poco, pero me base en distintos video tutoriales y paginas de la cuales me nutri de infromación.

Se me complico a la hora de hacer el llamado de los componentes y las rutas, pero lo asimile un poco con los demás proyectos que había realizado y pude captar la información.



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
