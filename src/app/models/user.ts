export interface User {
    fullName: string;
    document: string;
    cellPhone: string;
    email: string;
    password: string;
    departament: string;
    city: string;
    neighborhood: string;
    address: string;
    salary: BigInteger;
    otherMoneyIncome: BigInteger;
    monthlyExpenses: BigInteger;
    financialExpenses: BigInteger;

}
