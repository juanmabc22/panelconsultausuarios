import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { User } from '../models/user';
import { Login } from '../models/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiURL = environment.apiURL;
  constructor(private http: HttpClient) { }

  register(user:User): Observable<User>{
    return this.http.post<User>(`${this.apiURL}users`,user);
  }

  login(login1:Login): Observable<any>{
    return this.http.post(`${this.apiURL}login`, login1);
  }
}
