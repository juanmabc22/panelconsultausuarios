import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm!:FormGroup;
  user!:User;
  constructor(private fb:FormBuilder, private authService:AuthService) {
    this.createForm();
   }

  ngOnInit(): void {
  }

  register(): void{
    if(this.registerForm!.invalid){
      return Object.values(this.registerForm.controls).forEach(control => {
        control.markAsUntouched();
      });
    }else{
      this.setUser();
      this.authService.register(this.user).subscribe((data: any) => {
        console.log('registro completado');
      }, error => {
        console.error(error);
        
      });
    }
    
  }

  createForm():void{
    this.registerForm = this.fb.group({
      fullName: ['', [Validators.required]],
      document: ['', [Validators.required]],
      cellPhone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      password:  ['', [Validators.required]],
      departament:  ['', [Validators.required]],
      city:  ['', [Validators.required]],
      neighborhood:  ['', [Validators.required]],
      address:  ['', [Validators.required]],
      salary:  ['', [Validators.required]],
      otherMoneyIncome:  ['', [Validators.required]],
      monthlyExpenses:  ['', [Validators.required]],
      financialExpenses: ['', [Validators.required]],
    });
  }

  //Estos metodos me retornaran un verdadero o falso, para hacer la validación de cada uno de los campos
  get fullNameValidate(){
    return(
      this.registerForm.get('fullName')?.invalid && this.registerForm.get('fullName')?.touched
    );
  }

  get documentValidate(){
    return(
      this.registerForm.get('document')?.invalid && this.registerForm.get('document')?.touched
    );
  }

  get cellPhoneValidate(){
    return(
      this.registerForm.get('cellPhone')?.invalid && this.registerForm.get('cellPhone')?.touched
    );
  }

  get emailValidate(){
    return(
      this.registerForm.get('email')?.invalid && this.registerForm.get('email')?.touched
    );
  }

  get passwordValidate(){
    return(
      this.registerForm.get('password')?.invalid && this.registerForm.get('password')?.touched
    );
  }
  
  get departamentValidate(){
    return(
      this.registerForm.get('departament')?.invalid && this.registerForm.get('departament')?.touched
    );
  }

  get cityValidate(){
    return(
      this.registerForm.get('city')?.invalid && this.registerForm.get('city')?.touched
    );
  }

  get neighborhoodValidate(){
    return(
      this.registerForm.get('neighborhood')?.invalid && this.registerForm.get('neighborhood')?.touched
    );
  }

  get addressValidate(){
    return(
      this.registerForm.get('address')?.invalid && this.registerForm.get('address')?.touched
    );
  }

  get salaryValidate(){
    return(
      this.registerForm.get('salary')?.invalid && this.registerForm.get('salary')?.touched
    );
  }

  get otherMoneyIncomeValidate(){
    return(
      this.registerForm.get('otherMoneyIncome')?.invalid && this.registerForm.get('otherMoneyIncome')?.touched
    );
  }

  get monthlyExpensesValidate(){
    return(
      this.registerForm.get('monthlyExpenses')?.invalid && this.registerForm.get('monthlyExpenses')?.touched
    );
  }

  get financialExpensesValidate(){
    return(
      this.registerForm.get('financialExpenses')?.invalid && this.registerForm.get('financialExpenses')?.touched
    );
  }

  setUser(): void{
    this.user = {
      fullName:this.registerForm.get('fullName')?.value,
      document:this.registerForm.get('document')?.value,
      cellPhone:this.registerForm.get('cellPhone')?.value,
      email:this.registerForm.get('email')?.value,
      password:this.registerForm.get('password')?.value,
      departament:this.registerForm.get('departament')?.value,
      city:this.registerForm.get('city')?.value,
      neighborhood:this.registerForm.get('neighborhood')?.value,
      address:this.registerForm.get('address')?.value,
      salary:this.registerForm.get('salary')?.value,
      otherMoneyIncome:this.registerForm.get('otherMoneyIncome')?.value,
      monthlyExpenses:this.registerForm.get('monthlyExpenses')?.value,
      financialExpenses:this.registerForm.get('financialExpenses')?.value
    }
  }
}
